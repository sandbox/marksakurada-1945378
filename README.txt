A quick module to add support for pixi.js

a nice light weight html5 2d engine by GoodBoyDigital

https://github.com/GoodBoyDigital/pixi.js
http://www.goodboydigital.com/blog/
http://www.goodboydigital.com/pixijs/docs/


To install enable module and download pixi.js
to use the Drush make file from you docroot run

drush make sites/all/modules/pixijs/pixijs.make --no-core -y
